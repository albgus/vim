execute pathogen#infect()
syntax on
filetype plugin indent on

" Make comments more readable on black backgrounds
" highlight comment ctermfg=cyan

" Some sensible default indention settings
set ts=2
set sw=2
set backspace=indent,eol,start
set autoindent
set expandtab
set ruler

" Colors!
" set t_Co=256
" set t_ut=
colorscheme codedark
let g:airline_theme = 'codedark'

" Highlight extra whitespace at end of lines
highlight LiteralTabs ctermbg=darkgreen guibg=darkgreen
match LiteralTabs /\s\  /
highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
match ExtraWhitespace /\s\+$/

"" ansible-vim
" Detect ansible inventory better
au BufRead,BufNewFile inventory setfiletype ansible_hosts
" Highlightning options
let g:ansible_name_highlight = 'd'
let g:ansible_attribute_highlight = 'ab'
let g:ansible_extra_keywords_highlight = 1

" Enable expansion of space and CRs
let delimitMate_expand_cr = 2
let delimitMate_expand_space = 1

" Don't show mode with vim-airline
set showmode!

" vim-terraform
let g:terraform_align=1 " Note: requires vim-tabularize

" Python settings
au FileType python set ts=4 sts=4 sw=4 expandtab
au FileType nginx set ts=4 sts=4 sw=4 expandtab
