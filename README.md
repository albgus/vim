
Install configuration
---------------------

    cd ~
    git clone https://gitlab.com/albgus/vim.git .vim
    ln -s .vim/vimrc .vimrc
    cd ~/.vim
    git submodule init && git submodule update

Add a new plugin
----------------

    cd ~/.vim
    git submodule add http://github.com/vimrepo.git bundle/vimrepo
    git add .
    git commit
